{ mkDerivation, base, bytestring, cmdargs, cryptonite, saltine
, stdenv
}:
mkDerivation {
  pname = "forestwealth-import";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base bytestring cmdargs cryptonite saltine
  ];
  license = stdenv.lib.licenses.unfree;
}
