{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import System.Console.CmdArgs
import Control.Exception
import Crypto.Saltine
import Crypto.Saltine.Class
import Crypto.Saltine.Core.Sign
import Crypto.Hash
import Data.Maybe (fromJust)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as C

data App = Generate
         | Sign { keyPair :: FilePath, blob :: FilePath }
         | Verify { keyPair :: FilePath, blob :: FilePath, signature :: FilePath }
         | Main { keyPair :: FilePath, blob :: FilePath, signature :: FilePath }
         deriving (Show, Data, Typeable)

generate = Generate
signm = Sign{keyPair = def, blob = def}
verify = Verify{keyPair = def, blob = def, signature = def}
mainm = Main{keyPair = def, blob = def, signature = def}

main :: IO ()
main = do
    BS.putStr =<< app =<< cmdArgs (modes [generate, signm, verify, mainm])

app Generate = do
    (sec, pub) <- newKeypair
    return $ BS.concat [encode sec, encode pub] -- secret key is first 64 bytes

app Sign{..} = do
    f <- BS.readFile blob
    sec <- fromJust . (\x -> decode x :: Maybe SecretKey) . (BS.take 64) <$> (BS.readFile keyPair)
    return $ signDetached sec f

app Verify{..} = do
    f <- BS.readFile blob
    sig <- BS.readFile signature
    pub <- fromJust . (\x -> decode x :: Maybe PublicKey) . (BS.drop 64) <$> (BS.readFile keyPair)
    return $ C.pack $ show $ signVerifyDetached pub sig f

app Main{..} = do
    f <- BS.readFile blob
    sig <- BS.readFile signature
    pub <- fromJust . (\x -> decode x :: Maybe PublicKey) . (BS.drop 64) <$> (BS.readFile keyPair)
    assert (signVerifyDetached pub sig f) $ do
        let h = show $ (hash f :: Digest SHA256)
        BS.writeFile h f
        return $ C.pack h
